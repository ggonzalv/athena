#include "TrigValAlgs/TrigCountDumper.h"
#include "TrigValAlgs/TrigDecisionChecker.h"
#include "TrigValAlgs/TrigEDMChecker.h"
#include "TrigValAlgs/TrigEDMAuxChecker.h"
#include "TrigValAlgs/TrigProblemFinder.h"
#include "TrigValAlgs/TrigSlimValAlg.h"

DECLARE_COMPONENT( TrigCountDumper )
DECLARE_COMPONENT( TrigDecisionChecker )
DECLARE_COMPONENT( TrigEDMChecker )
DECLARE_COMPONENT( TrigEDMAuxChecker )
DECLARE_COMPONENT( TrigProblemFinder )
DECLARE_COMPONENT( TrigSlimValAlg )

